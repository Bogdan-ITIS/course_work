import argparse
import codecs
import pickle

import xml.etree.ElementTree as ET
import pandas as pd
import numpy as np
from lxml import objectify
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import cross_val_score


def load_model(file):
    with open(file, 'rb') as pickle_file:
        model = pickle.load(pickle_file)
    return model

def load_data(input):
    with codecs.open(input, 'r', "utf_8") as file:
        data = objectify.fromstring(file.read())
    dic_X = {}
    dic_meyer = {}
    dic_sp = {}
    first_it = True
    for doc in data.corpus.course:
        if first_it:
            for value in doc.value:
                if value.attrib['name'] == 'meyer_complexity_score':
                    dic_meyer['meyer_complexity_score'] = [value]
                elif value.attrib['name'] == 'story_point_complexity_score':
                    dic_sp['story_point_complexity_score'] = [value]
                else:
                    dic_X[str(value.attrib['name'])] = [value]
            first_it = False
        for value in doc.value:
            if value.attrib['name'] == 'meyer_complexity_score':
                dic_meyer['meyer_complexity_score'].append(value)
            elif value.attrib['name'] == 'story_point_complexity_score':
                dic_sp['story_point_complexity_score'].append(value)
            else:
                dic_X[str(value.attrib['name'])].append(value)
    dic_X.pop('course_name')
    class_data = pd.DataFrame(dic_X)
    X = class_data.values
    class_data = pd.DataFrame(dic_meyer)
    y_meyer = class_data['meyer_complexity_score'].values
    class_data = pd.DataFrame(dic_sp)
    y_sp = class_data['story_point_complexity_score'].values
    return X, y_meyer, y_sp

def main(parser):
    args = parser.parse_args()

    # Количество частей, на которые будет делиться выборка при cross-validation
    num_folds = 5

    # Загрузка обученной модели и предсказание на входных данных
    if (args.mode == 'use'):
        data = load_data(args.input)[0]
        model = load_model(args.model)
        predicted = model.predict(data)
        with open('../out.txt', 'a+', encoding=args.encoding) as out:
            for p in predicted:
                out.write(str(p) + "\n")

    # Обучение модели
    elif (args.mode == 'train'):
        data, y_mayer, y_sp = load_data(args.src_train)
        model = LinearRegression()
        model.fit(data, y_sp)
        file = open(args.output, 'wb')
        pickle.dump(model, file)
        file.close()
        print("--- Training is complete ---")

    # Вычисление метрик на 5-folds-cross-validation
    elif (args.mode == 'test'):
        model = LinearRegression()
        X, y_mayer, y_sp = load_data(args.input)
        scores = cross_val_score(model, X, y_sp, cv=num_folds, scoring='neg_mean_squared_error')
        mse_scores = -scores
        rmse_scores = np.sqrt(mse_scores)
        with open('../out.txt', 'a+', encoding=args.encoding) as out:
            out.write(
                "--- Cross-validation on " + str(num_folds) + " folds ---\n")
            out.write("RMSE: " + str(rmse_scores.mean()))
        print("--- 5-folds-cross-validation is complete ---")

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--src-train-texts', action="store", dest="src_train", default='../test.xml')
    parser.add_argument('--text-encoding', action="store", dest="encoding", default="utf_8")
    #parser.add_argument('-o', action="store", dest="output", default='../out.txt')
    parser.add_argument('-o', action="store", dest="output", default='../model.pickle')
    parser.add_argument('-m', action="store", dest="model", default='../model.pickle')
    parser.add_argument('-i', action="store", dest="input", default='../test.xml')
    parser.add_argument('--mode', choices=['test', 'train', 'use'], action="store", default='test')

    main(parser)
